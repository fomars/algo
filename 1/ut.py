__author__ = 'Arseniy'
import unittest
import merge_sort
from merge_sort import Array

class TestRecursion(unittest.TestCase):

    def setUp(self):
        pass


    def test_small(self):
        a = Array([4,2,5,1])
        self.failUnless(a.sort().matches([1,2,4,5]))

    def test_medium(self):
        a = Array([5,8,6,3,1,9,2,4,0,7])
        self.failUnless(a.sort().matches([0,1,2,3,4,5,6,7,8,9]))


class TestCounter(unittest.TestCase):

    def test1(self):

        pass

class TestMerge(unittest.TestCase):

    def setUp(self):
        pass


    def test_merge_small_1(self):
        a1 = [1,3]
        a2 = [2]
        self.assertEqual(Array.merge(a1,a2),[1,2,3])

    def test_merge_small_2(self):
        a1 = [5]
        a2 = [1,2]
        self.assertEqual(Array.merge(a1,a2),[1,2,5])

    def test_merge_small_3(self):
        a1 = [5]
        a2 = [8,10]
        self.assertEqual(Array.merge(a1,a2),[5,8,10])

    def test_merge_small_4(self):
        a1 = [5,7]
        a2 = [4]
        self.assertEqual(Array.merge(a1,a2),[4,5,7])

    def test_merge_small_5(self):
        a1 = [5,7]
        a2 = [8]
        self.assertEqual(Array.merge(a1,a2),[5,7,8])

    def test_merge_big_11(self):
        a1 = [1,6,10,22]
        a2 = [5,12,13,15]
        self.assertEqual(Array.merge(a1,a2),[1,5,6,10,12,13,15,22])


    def test_merge_big_12(self):
        a1 = [5,6,10]
        a2 = [11,12,20,22]
        self.assertEqual(Array.merge(a1,a2),[5,6,10,11,12,20,22])


    def test_merge_big_21(self):
        a1 = [5,6,10,22]
        a2 = [1,12,13,15]
        self.assertEqual(Array.merge(a1,a2),[1,5,6,10,12,13,15,22])


    def test_merge_big_22(self):
        a1 = [5,6,10,15]
        a2 = [1,12,13,22]
        self.assertEqual(Array.merge(a1,a2),[1,5,6,10,12,13,15,22])




