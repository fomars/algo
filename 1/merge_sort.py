__author__ = 'Arseniy'


class Array(object):

    def __init__(self, a):
        self._list = a
        self.inversions = 0
        self.pop = self._list.pop
        # self.__getitem__ = self._list.__getitem__

    def __getitem__(self, item):
        return self._list.__getitem__(item)

    def matches(self, other):
        print self._list
        print other
        return self._list == other

    def sort(self):
        _len = len(self)
        if _len > 2:
            a1, a2 = self.split()
            a = Array(self.merge(a1.sort(),a2.sort()))
            return a
        elif _len == 2:
            return self.sort_two()
        else:
            return self

    def __len__(self):
        return len(self._list)

    def split(self):
        _len = len(self)
        a1 = Array(self._list[:_len/2])
        a2 = Array(self._list[_len/2:])
        return a1, a2


    @staticmethod
    def merge(a1, a2):
        assert isinstance(a1, (Array, list))
        assert isinstance(a2, (Array, list))
        a = []
        while True:
            try:
                i1 = a1[0]
                try:
                    i2 = a2[0]
                    while i2 <= i1:
                        a.append(a2.pop(0))

                        i2 = a2[0]
                except IndexError:
                    pass
                a.append(a1.pop(0))
            except IndexError:
                while True:
                    try:
                        a.append(a2.pop(0))
                    except IndexError:
                        break
                break

        return a

    def sort_two(self):
        assert len(self)==2
        a = [min(self._list), max(self._list)]
        return Array(a)


if __name__ == "__main__":
    inp_file = open("input.txt", "r")
    array = [int(c) for c in inp_file.readlines()]
    # print array
